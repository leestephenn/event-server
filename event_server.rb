require 'date' # for parse method
require 'icalendar/recurrence'
require "webrick"
require 'time'
#require 'net/http'
#require 'net/https'
#require 'excon'

cal_files = [ File.open("Classes.ics"), File.open("Events.ics")]

$events = []
puts $events;
cal_files.each do |cal_file| 
    calendars = Icalendar::Calendar.parse(cal_file) # parse an ICS file
    $events = $events + calendars.first.events 
end

$id = 1

def events_between(date1, date2)
    result = "[\n"
    $events.each do |e|
	ooo = e.occurrences_between(date1, date2)
	#puts "Number of occurences: #{ooo.size}"
	if ooo.size > 0
	    ooo.each do |o| 
		if result != "[\n"
		    result << ",\n"
		end
		result << "{"
		result << "\"title\": \"#{e.summary}\", "
		result << "\"id\": #{$id}, "
		$id = $id + 1
		result << "\"start\": \"#{o.start_time.iso8601}\", "
		result << "\"end\": \"#{o.end_time.iso8601}\" "
		result << "}"
	    end
	end
    end
    result << "\n]"
    return result
end

class MyServlet < WEBrick::HTTPServlet::AbstractServlet
    def do_GET (request, response)
	if request.query["start"] && request.query["end"]
            a = request.query["start"]
	    b = request.query["end"]
            #puts a, b
            response.status = 200
            response.content_type = "application/json"
            result = nil
            
	    result = events_between(Date.parse(a), Date.parse(b))
	    #puts response.class
            
            response.body = result.to_s + "\n"
        else
            response.status = 200
            response.body = "You did not provide the correct parameters"
        end
    end
end

server = WEBrick::HTTPServer.new(:Port => 1234, :DocumentRoot => "webpage")

server.mount "/calendar", MyServlet

trap("INT") {
    server.shutdown
}

#puts "Here"

server.start
