# event-server

This was an early attempt to automatically load events from my Google calendar into a daily schedule. Its primary component is a Ruby script that reads an iCalendar file and outputs events in JSON form. Those events are read by an installation of the [FullCalendar Javascript calendar](https://fullcalendar.io/) in the index.html file in the webpage subdirectory.

This was tested with an installation of Ruby 2.6.2

## Install Dependencies

This project uses several Ruby libraries as dependencies. To install those using Gem, run:

    gem install "icalendar-recurrence" tzinfo

## Running

Once the dependencies are installed, you can run the program with the command

    ruby event_server.rb

This may take a bit of time to run, since it must parse the iCalendar files.
Once you see the message "INFO  WEBrick 1.4.2"  go to <http://localhost:1234/>
in a web browser. This will show you events, taken from the sample data in the
Classes.ics and Events.ics files. Click on the arrows on the top right to go
earlier and later.
